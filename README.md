# Characterization of habitat requirements of European fishing spiders

This dataset and Rcodes correspond to the manuscript: "Characterization of habitat requirements of European fishing spiders"

Authors: Lisa Dickel¹², Jérémy Monsimet², Denis Lafage³⁴, Olivier Devineau²  

1 Centre for Biodiversity Dynamics, Department of Biology, Norwegian University of Sciences and Technology (NTNU), Trondheim, Norway  
2 Department of Forestry and Wildlife management, Inland Norway University of Applied Sciences, Campus Evenstad, Koppang, Norway  
3 CNRS, ECOBIO (Ecosystèmes, biodiversité, évolution) - UMR 6553, University of Rennes, F 35000 Rennes, France  
4 Department of Environmental and Life Sciences/Biology, Karlstad University, Karlstad, Sweden  

If you have questions on statistical analyses, contact:

- [lisa.dickel@ntnu.no](lisa.dickel@ntnu.no)
- [jeremy.monsimet@posteo.net](jeremy.monsimet@posteo.net)

## Content

- Data/: folder with data on nurseries and on habitats at the site scale
- FDA.rmd: file to reproduce the **Flexible discrimiant analysis (FDA)** to compare different sites among *Dolomedes* occupation status (*D.plantarius*/*D.fimbriatus*/both species/no species). It is possible to make the figure 3 with this script.
- GAM.rmd: file to reproduce the **binomial Generalized Additive Model (GAM)** to characterise microhabitats around nursery. It is possible to make the figures 4 and 5 with this script.
- html files: html output of the .rmd files
